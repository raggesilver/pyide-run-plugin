# Run plugin for [PyIDE](https://gitlab.com/raggesilver/PyIDE)

This plugin creates a play button on the header bar that allows you to run a set of commands with one click. It allows you to specify the commands for each project. Good for compiling, building, running and possibly debugging your project.

## Features

- Run defined commands
- Print output on `bottom_panel.output_text_view`
- Quick run pressing `F5`

## Change "log"

- Core functionality

## TODO

- Interface to create `.pyide/settings.json` if it doesn't exist