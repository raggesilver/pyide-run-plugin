"""PyIDE git plugin."""
import os
import json
import subprocess

from enum import Enum
from gi.repository import GObject, Gtk, GLib, Gdk


class PluginState(Enum):
    """Plugin state enum."""

    IDLE = 0
    RUNNING = 1


class Plugin(GObject.GObject):
    """Plugin class."""

    def __init__(self, *args, **kwargs):
        """Docstrings default."""
        GObject.GObject.__init__(self)

        if 'application_window' in kwargs:
            self.application_window = kwargs['application_window']
            self.path = self.application_window.tree_view.path

        if 'settings' in kwargs:
            self.settings = kwargs['settings']

        self.state = PluginState.IDLE

        # Play icon
        self.play_img = Gtk.Image.new_from_icon_name(
            'media-playback-start-symbolic', Gtk.IconSize.MENU)
        self.play_img.show()

        # Stop icon
        self.stop_img = Gtk.Image.new_from_icon_name(
            'media-playback-stop-symbolic', Gtk.IconSize.MENU)
        self.stop_img.show()

    def do_activate(self, *args):
        """Docstrings default."""
        if not self.application_window or not self.settings:
            return

        self.project_settings_path = os.path.join(self.path,
                                                  ".pyide",
                                                  "settings.json")

        self.thread_manager =\
            self.application_window.get_application().thread_manager

        self.play_button = Gtk.Button()
        self.play_button.add(self.play_img)
        self.play_button.connect('clicked', self._on_button_clicked)

        self.play_button.show()

        self.application_window.header_bar.pack_start(self.play_button)

        # Make this an option
        self.application_window.accel_group.connect(Gdk.keyval_from_name('F5'),
                                                    0,
                                                    0,
                                                    self.run_default)

        self.check_project_settings()

    # THIS IS A THREADED FUNTION
    def _do_check_project_settings(self, *args):
        """Check if the .pyide/settings.json file exists."""
        if os.path.isfile(self.project_settings_path):

            # Project settings exists
            with open(self.project_settings_path, 'r') as f:

                try:
                    self.project_settings = json.load(f)
                    # If the run array is present and not empty
                    if ('run' in self.project_settings
                            and len(self.project_settings['run']) > 0):
                        self._can_run = True
                    else:
                        self._can_run = False
                except Exception:
                    GLib.idle_add(self._append_to_output,
                                  "Reading settings.json failed.\n",
                                  True)
                    self._can_run = False

        else:
            self._can_run = False

        return self._can_run

    def _check_project_settings_cb(self, *args):
        print('Can run: %s' % self._can_run)
        print(args)

    def check_project_settings(self, *args):
        """Call the actual function in a thread."""
        self.thread_manager.make_thread(self._check_project_settings_cb,
                                        self._do_check_project_settings,
                                        [],
                                        'project_settings_thread')

    def run_default(self, *args):
        """Call the actual function in a thread."""
        self._do_check_project_settings()
        if not self._can_run:
            return

        self.state = PluginState.RUNNING
        self._apply_state()

        self.application_window.bottom_panel.show_output()

        self.thread_manager.make_thread(self._run_default_cb,
                                        self._do_run_default,
                                        [],
                                        'run_thread')

    # THIS IS A THREADED FUNTION
    def _do_run_default(self, *args):

        # self._do_check_project_settings()
        # self._check_project_settings_cb()

        if not self._can_run:
            return

        self._terminated = False

        if 'default' in self.project_settings:
            print('call %s' % self.project_settings['default'])
        else:

            for command in self.project_settings['run']:

                if self._terminated:
                    break

                _command = command

                # Convert command type to str if list
                # else return None
                if type(_command) is str:
                    pass

                elif type(_command) is list:
                    _command = " ".join(_command)

                else:
                    continue

                # Show what command was ran
                GLib.idle_add(self._append_to_output, _command + "\n")

                self.process = subprocess.Popen(_command,
                                                shell=True,
                                                stdout=subprocess.PIPE,
                                                stderr=subprocess.STDOUT,
                                                preexec_fn=os.setsid)

                # Get command output and call append function in main thread
                for line in iter(self.process.stdout.readline, b''):
                    GLib.idle_add(self._append_to_output, line)

                # Wait for the process to exit
                self.process.wait()

                # Send the return code string
                GLib.idle_add(self._append_to_output,
                              "Return code: %s\n" % self.process.returncode)

    def _run_default_cb(self, *args):
        self.state = PluginState.IDLE
        self._apply_state()

    def kill_subprocess(self, *args):
        """Kill the running process."""
        if hasattr(self, 'process'):
            # os.killpg(os.getpgid(self.process.pid), signal.SIGTERM)
            self._terminated = True
            self.process.terminate()

    def _append_to_output(self, text, force_show=False):
        self.application_window.bottom_panel.append_to_output(text)
        if force_show:
            self.application_window.bottom_panel.show_output()

    def _apply_state(self, *args):

        print("State: %s" % self.state)
        self.play_button.remove(self.play_button.get_child())

        if self.state == PluginState.IDLE:
            self.play_button.add(self.play_img)
        elif self.state == PluginState.RUNNING:
            self.play_button.add(self.stop_img)

    def _on_button_clicked(self, *args):
        if self.state == PluginState.IDLE:
            self.run_default()
        elif self.state == PluginState.RUNNING:
            self.kill_subprocess()
